Pod::Spec.new do |s|
  s.name        = "SwiftResponseParser"
  s.version     = "1.0"
  s.summary     = "Simple response parser for Swift"
  s.license     = { :type => "MIT" }
  s.authors     = { "ducker" => "leszekducker@gmail.com" }

  s.requires_arc = true
  s.ios.deployment_target = "8.0"
  s.source   = { :git => "https://bitbucket.org/ducker/swiftresponseparser"}
  s.source_files = "SwiftResponseParser/ResponseParser/*.swift"
end
