//
//  AbstractROM.swift
//  CompanyManagerAPI
//
//  Created by Leszek Kaczor on 22/10/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

import UIKit

public
class AbstractROM: NSObject {
    
    public class func getProperties() -> [String] {
        let currentClass = self
        var names: [String] = []
        var count: UInt32 = 0
        var properties = class_copyPropertyList(currentClass, &count)
        for var i = 0; i < Int(count); ++i {
            let property: objc_property_t = properties[i]
            let attributes: String = NSString(CString: property_getAttributes(property), encoding: NSUTF8StringEncoding)!
            if !attributes.componentsSeparatedByString(",").filter({ $0 == "R"}).isEmpty {
                continue
            }
            let name: String = NSString(CString: property_getName(property), encoding: NSUTF8StringEncoding)!
            names.append(name)
        }
        free(properties)
        let superClass: AnyClass! = class_getSuperclass(currentClass)
        if superClass.isSubclassOfClass(AbstractROM.self) {
            names += superClass.getProperties()
        }
        return names
    }
 
    public override required init() {
        super.init()
    }
    
    public class func getCustomClassForPropertyName(name:String) -> AnyClass! {
        return NSNull.self
    }
    
    public func customMappingForPropertyName(propertyName:String, value:AnyObject) -> Bool {
        return false
    }
    
    public class func getRootObjectKey() -> String? {
        return nil
    }
    
    public class func customMappingDictionary() -> [String:String]? {
        return ["objectId" : "id"]
    }
}
