//
//  ResponseParser.swift
//  CompanyManagerAPI
//
//  Created by Leszek Kaczor on 22/10/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

import UIKit

public
class ResponseParser: NSObject {
    
    private class Configuration {
        var logTime = false
        var dateFormatter: NSDateFormatter! = nil
        
        class var sharedInstance : Configuration {
            struct Static {
                static let instance : Configuration = Configuration()
            }
            return Static.instance
        }
    }
   
    public class func parseResponse(response:AnyObject!, usingClass parseClass:AnyClass) -> AnyObject! {
        if response == nil { return nil }
        if parseClass is NSNull.Type { return nil }
        var currentResponse:AnyObject! = response
        let date = NSDate()
        if currentResponse is NSString {
            currentResponse = currentResponse.dataUsingEncoding(NSUTF8StringEncoding)
        }
        if currentResponse != nil && currentResponse is NSData {
            currentResponse = NSJSONSerialization.JSONObjectWithData(currentResponse as NSData, options: NSJSONReadingOptions.allZeros, error: nil)
        }
        let result: AnyObject! = parseResponseObjects(currentResponse, usingClass: parseClass)
        if Configuration.sharedInstance.logTime {
            print("Parse class object: \(parseClass) time taken: \(NSDate().timeIntervalSinceDate(date))")
        }
        return result
    }
    
    public class func logTime(log:Bool) {
        Configuration.sharedInstance.logTime = log
    }
    
    public class func dateFormatter(formatter:NSDateFormatter!) {
        Configuration.sharedInstance.dateFormatter = formatter
    }
    
    class func parseResponseObjects(response:AnyObject!, usingClass parseClass:AnyClass) -> AnyObject! {
        if response == nil {
            return response
        }
        if !(parseClass is AbstractROM.Type) {
            return response
        }
        var result: AnyObject! = response
        if response is NSArray {
            var responseArray: [AnyObject] = []
            for obj in (response as NSArray) {
                let newObject: AnyObject! = createObjectOfClass(parseClass as AbstractROM.Type, fromDictionary: (obj as [String:AnyObject]))
                if newObject != nil {
                    responseArray.append(newObject)
                }
            }
            result = responseArray
        } else {
            result = createObjectOfClass(parseClass as AbstractROM.Type, fromDictionary: (result as [String:AnyObject]))
        }
        return result
    }
    
    class func createObjectOfClass(parseClass:AbstractROM.Type, fromDictionary dict:[String:AnyObject]) -> AnyObject! {
        var newObject: AnyObject! = nil
        var dictionaryObject = dict
        if let rootKey:String = parseClass.getRootObjectKey() {
            if dictionaryObject[rootKey] != nil {
                dictionaryObject = dictionaryObject[rootKey] as [String:AnyObject]
            }
        }
        
        for propertyName in parseClass.getProperties() {
            let customClass: AnyClass! = parseClass.getCustomClassForPropertyName(propertyName)
            var value: AnyObject! = getValueForPropertyNamed(propertyName, fromDictionary: dictionaryObject, forClass: parseClass)
            value = parseResponseObjects(value, usingClass: customClass)
            if value != nil && !value.isKindOfClass(NSNull.self) {
                if newObject == nil { newObject = parseClass() }
                if (newObject as AbstractROM).customMappingForPropertyName(propertyName, value: value) {
                } else if Configuration.sharedInstance.dateFormatter != nil && isDatePropertyNamed(propertyName, forClass: parseClass) {
                    newObject.setValue(Configuration.sharedInstance.dateFormatter.dateFromString(value as String), forKey: propertyName)
                } else {
                    newObject.setValue(value, forKey: propertyName)
                }
            }
        }
        
        return newObject
    }
    
    class func isDatePropertyNamed(name:String, forClass parseClass:AnyClass) -> Bool {
        let property = class_getProperty(parseClass, name)
        let attributes:String = NSString(CString: property_getAttributes(property), encoding: NSUTF8StringEncoding)!
        if attributes.rangeOfString("NSDate") != nil {
            return true
        }
        return false
    }
    
    class func getValueForPropertyNamed(name:String, fromDictionary dict:[String:AnyObject], forClass parseClass:AbstractROM.Type) -> AnyObject! {
        struct RegexHolder {
            static let regex = NSRegularExpression(pattern: "([a-z])([A-Z]+)", options: NSRegularExpressionOptions.allZeros, error: nil)!
        }
        if let key = parseClass.customMappingDictionary()?[name] {
            return dict[key]
        }
        var result: AnyObject! = dict[name]
        if result == nil {
            var replaced = RegexHolder.regex.stringByReplacingMatchesInString(name, options: NSMatchingOptions.allZeros, range: NSMakeRange(0, countElements(name)), withTemplate: "$1_$2")
            replaced = replaced.lowercaseString
            result = dict[replaced]
        }
        return result
    }
    
}
