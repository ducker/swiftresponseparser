## SwiftResponseParser

This is the repository for SwiftResponseParser.

## Installation

Add following line to your Podfile

```
pod 'SwiftResponseParser', :git => "https://bitbucket.org/ducker/swiftresponseparser.git"
```

If no cocoapods installed go to http://cocoapods.org for further informations.

## Usage

### Basic usage

Your model class must be a AbstractROM subclass. If your JSON looks like:
```json
{
   "string_property":"some text",
   "number_property":100,
   "array_property":[
      "text1",
      "text2",
      "text3"
   ],
   "some_date":"2015-01-6T11:15:52Z",
   "bool_flag":true
}
```

Your model class should be (you can use either property_name and propertyName style)

```swift
public class MyModelROM : AbstractROM {

    public var stringProperty: String = "";
    public var numberProperty: Int = -1;
    public var boolFlag: Bool = false;
    public var arrayProperty: [String]!;
    public var someDate: NSDate!;
}
```

SwiftResponseParser uses Objective-C runtime classes, so you cannot use types and values that are unknown for Objective-C. You cannot use optionals, so every struct based type like String, Int etc must have initial value. Class based types like NSDate, arrays dictionaries can be nil as explicitly unwrapped optional (type with exclamation mark).

It's all you have to do with your model. If you use NSDate you can specify date formatter for ResponseParser

```swift
let dateFormatter = NSDateFormatter()
dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
ResponseParser.dateFormatter(dateFormatter)
```

And that's it. You can parse JSON using SwiftResponseParser. JSON can be NSDictionary, NSArray, NSString or NSDate.

```swift
let result = ResponseParser.parseResponse(JSON, usingClass: MyModelROM.self) as? MyModelROM
```

If your response returns array of objects the only thing you have to change is responseObject type

```swift
let resultArray = ResponseParser.parseResponse(JSON, usingClass: MyModelROM.self) as? [MyModelROM]
```


### Advanced usage

You can specify which properties should be parsed by overriding getProperties method.

```swift
    public override class func getProperties() -> [String] {
        var properties = super.getProperties()
        properties = properties.filter({ $0 != "stringProperty" })
        return properties;
    }
```

If your model class contains another model as property you have to specify it in getCustomClassForPropertyName method. If you don't do it it will pass NSDictionary object (or NSDictionary objects array).

```swift
class MyAnotherModelROM : AbstractROM {

    public var myModelObject: MyModelROM!
    public var myModelObject: myModelObjectArray!

    public override class func getCustomClassForPropertyName(name:String) -> AnyClass! {
        switch name {
        case "myModelObject", "myModelObjectArray":
            return MyModelROM.self
        default:
            break
        }
        return super.getCustomClassForPropertyName(name)
    }

}

```

Method getRootObjectKey can be used for example if JSON has root key before objects and we want to have just object without it.

```json
[
   {
      "object":{
         "string_property":"some text",
         "number_property":100,
         "array_property":[
            "text1",
            "text2",
            "text3"
         ],
         "some_date":"2015-01-6T11:15:52Z",
         "bool_flag":true
      }
   },
   {
      "object":{
         "string_property":"some text",
         "number_property":100,
         "array_property":[
            "text1",
            "text2",
            "text3"
         ],
         "some_date":"2015-01-6T11:15:52Z",
         "bool_flag":true
      }
   },
   {
      "object":{
         "string_property":"some text",
         "number_property":100,
         "array_property":[
            "text1",
            "text2",
            "text3"
         ],
         "some_date":"2015-01-6T11:15:52Z",
         "bool_flag":true
      }
   }
]
```

You should always use super method if not returning custom value, because you can inherits from your models.

```swift-c
    class ExtendedModelROM : MyModelROM {

        public var anotherStringProperty: String = ""
        public var anotherNumberProperty: Int = -1

    }
```

### Custom mapping

If you want to use different key that in your JSON object you should specify it in customKeyMappingDictionary method. Default mapping prevents from using id and description fields of NSObject.

```swift
    public class func customMappingDictionary() -> [String:String]? {
        return ["objectId" : "id"]
    }
```

Custom property mapping is provided by customMappingForPropertyName:value: method.

```swift
    public override func customMappingForPropertyName(propertyName: String, value: AnyObject) -> Bool {
        if propertyName == "stringProperty" {
            stringProperty = value.capitalizedString
            return true
        }
        return super.customMappingForPropertyName(propertyName, value: value)
    }
```


## Updating

### Notes