//
//  SwiftResponseParserTests.swift
//  SwiftResponseParserTests
//
//  Created by Leszek Kaczor on 09/02/15.
//  Copyright (c) 2015 Leszek Kaczor. All rights reserved.
//

import UIKit
import XCTest
import SwiftResponseParser

class SwiftResponseParserTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSingleObject() {
        let filepath = NSBundle.mainBundle().pathForResource("SingleTopic", ofType: "json")
        let jsonString = NSString(contentsOfFile: filepath!, encoding: NSUTF8StringEncoding, error: nil)
        let response: AnyObject? = NSJSONSerialization.JSONObjectWithData(jsonString!.dataUsingEncoding(NSUTF8StringEncoding)! , options: NSJSONReadingOptions.allZeros, error: nil)
        let responseObject: AnyObject! = ResponseParser.parseResponse(response, usingClass: TopicROM.self)
        XCTAssertTrue(responseObject is TopicROM, "Object is not kind of class TopicROM")
    }
    
    func testSingleObjectWithCapitalizedProperties() {
        let filepath = NSBundle.mainBundle().pathForResource("SingleTopic", ofType: "json")
        let jsonString = NSString(contentsOfFile: filepath!, encoding: NSUTF8StringEncoding, error: nil)
        let response: AnyObject? = NSJSONSerialization.JSONObjectWithData(jsonString!.dataUsingEncoding(NSUTF8StringEncoding)! , options: NSJSONReadingOptions.allZeros, error: nil)
        let responseObject: AnyObject! = ResponseParser.parseResponse(response, usingClass: TopicROM.self)
        let responseCapitalizedObject: AnyObject! = ResponseParser.parseResponse(response, usingClass: TopicCapitalizedROM.self)
        XCTAssertTrue(responseCapitalizedObject is TopicCapitalizedROM, "Object is not kind of class TopicROM")
    }
    
    func testArrayOfObjects() {
        let filepath = NSBundle.mainBundle().pathForResource("TopicResponseArray", ofType: "json")
        let jsonString = NSString(contentsOfFile: filepath!, encoding: NSUTF8StringEncoding, error: nil)
        let response: AnyObject = NSJSONSerialization.JSONObjectWithData(jsonString!.dataUsingEncoding(NSUTF8StringEncoding)! , options: NSJSONReadingOptions.allZeros, error: nil) as [AnyObject]
        let responseObject: [TopicROM]! = ResponseParser.parseResponse(response, usingClass: TopicROM.self) as? [TopicROM]
        XCTAssertTrue(responseObject.count == response.count, "Object is not king of class TopicROM")
    }
    
    func testArrayOfCapitalizedObjects() {
        let filepath = NSBundle.mainBundle().pathForResource("TopicResponseArray", ofType: "json")
        let jsonString = NSString(contentsOfFile: filepath!, encoding: NSUTF8StringEncoding, error: nil)
        let response: AnyObject = NSJSONSerialization.JSONObjectWithData(jsonString!.dataUsingEncoding(NSUTF8StringEncoding)! , options: NSJSONReadingOptions.allZeros, error: nil) as [AnyObject]
        let responseObject: [TopicCapitalizedROM]! = ResponseParser.parseResponse(response, usingClass: TopicCapitalizedROM.self) as? [TopicCapitalizedROM]
        XCTAssertTrue(responseObject.count == response.count, "Object is not king of class TopicCapitalizedROM")
    }
    
    func testParseFromNSString() {
        let filepath = NSBundle.mainBundle().pathForResource("SingleTopic", ofType: "json")
        let jsonString = NSString(contentsOfFile: filepath!, encoding: NSUTF8StringEncoding, error: nil)
        let responseObject: AnyObject! = ResponseParser.parseResponse(jsonString, usingClass: TopicCapitalizedROM.self)
        XCTAssertTrue(responseObject is TopicCapitalizedROM, "Object is not kind of class TopicCapitalizedROM")
    }
    
    func testParseFromNSData() {
        let filepath = NSBundle.mainBundle().pathForResource("SingleTopic", ofType: "json")
        let jsonString = NSString(contentsOfFile: filepath!, encoding: NSUTF8StringEncoding, error: nil)
        let jsonData = jsonString!.dataUsingEncoding(NSUTF8StringEncoding)
        let responseObject: AnyObject! = ResponseParser.parseResponse(jsonData, usingClass: TopicCapitalizedROM.self)
        XCTAssertTrue(responseObject is TopicCapitalizedROM, "Object is not kind of class TopicCapitalizedROM")
    }
    
    func testParseObjectWithDateFormatter() {
        let filepath = NSBundle.mainBundle().pathForResource("SingleTopic", ofType: "json")
        let jsonString = NSString(contentsOfFile: filepath!, encoding: NSUTF8StringEncoding, error: nil)
        let response: AnyObject? = NSJSONSerialization.JSONObjectWithData(jsonString!.dataUsingEncoding(NSUTF8StringEncoding)! , options: NSJSONReadingOptions.allZeros, error: nil)
        let responseObject: TopicROM! = ResponseParser.parseResponse(response, usingClass: TopicROM.self) as TopicROM
        XCTAssertNotNil(responseObject.created_at, "Created at property is nil")
    }
    
    func testNilResponse() {
        let responseObject: TopicROM! = ResponseParser.parseResponse(nil, usingClass: TopicROM.self) as? TopicROM
        XCTAssertNil(responseObject, "Response is not nil")
    }
    
    func testEmptyStringResponse() {
        let responseObject: TopicROM! = ResponseParser.parseResponse("", usingClass: TopicROM.self) as? TopicROM
        XCTAssertNil(responseObject, "Response is not nil")
    }
    
    func testCustomKeyMapping() {
        let filepath = NSBundle.mainBundle().pathForResource("SingleTopic", ofType: "json")
        let jsonString = NSString(contentsOfFile: filepath!, encoding: NSUTF8StringEncoding, error: nil)
        let response: AnyObject? = NSJSONSerialization.JSONObjectWithData(jsonString!.dataUsingEncoding(NSUTF8StringEncoding)! , options: NSJSONReadingOptions.allZeros, error: nil)
        let responseObject: TopicTypoROM! = ResponseParser.parseResponse(response, usingClass: TopicTypoROM.self) as TopicTypoROM
        XCTAssertNotNil(responseObject.nameuser, "Property is nil")
    }
    
    func testCustomPropertyMapping() {
        let filepath = NSBundle.mainBundle().pathForResource("SingleTopic", ofType: "json")
        let jsonString = NSString(contentsOfFile: filepath!, encoding: NSUTF8StringEncoding, error: nil)
        let response: AnyObject? = NSJSONSerialization.JSONObjectWithData(jsonString!.dataUsingEncoding(NSUTF8StringEncoding)! , options: NSJSONReadingOptions.allZeros, error: nil)
        let responseObject: TopicExtendedROM! = ResponseParser.parseResponse(response, usingClass: TopicExtendedROM.self) as TopicExtendedROM
        XCTAssertTrue(responseObject.isImage, "Property is nil")
    }
    
}
