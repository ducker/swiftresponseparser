//
//  TopicROM.swift
//  SwiftResponseParser
//
//  Created by Leszek Kaczor on 09/02/15.
//  Copyright (c) 2015 Leszek Kaczor. All rights reserved.
//

import UIKit

class TopicROM: AbstractROM {
    
    var active: Bool = false
    var comments_count: Int = -1
    var points: Int = -1
    var tags: [String]!
    var updated_at: NSDate!
    var views_count: Int = -1
    
    
    var created_at: NSDate!
    var discussion: String = ""
    var image: String = ""
    var old_id: Int = -1
    var permalink: String = ""
    var featured: Bool = false
    var username: String = ""
    var category_title: String = ""
    var ticckles_count: Int = -1
    var replies_count: Int = -1
    var user_presence: String = ""
    var user_old_id: Int = -1
    var user_avatar_url: String = ""
}
