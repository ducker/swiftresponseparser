//
//  TopicExtendedROM.swift
//  SwiftResponseParser
//
//  Created by Leszek Kaczor on 09/02/15.
//  Copyright (c) 2015 Leszek Kaczor. All rights reserved.
//

import UIKit

class TopicExtendedROM: TopicCapitalizedROM {
    var isImage: Bool {
        return image != ""
    }
    
    internal override class func getProperties() -> [String] {
        var properties = super.getProperties()
        properties = properties.filter({ $0 != "isImage" })
        return properties
    }
}
