//
//  TopicCapitalizedROM.swift
//  SwiftResponseParser
//
//  Created by Leszek Kaczor on 09/02/15.
//  Copyright (c) 2015 Leszek Kaczor. All rights reserved.
//

import UIKit

class TopicCapitalizedROM: AbstractROM {
    var active: Bool = false
    var commentsCount: Int = -1
    var points: Int = -1
    var tags: [String]!
    var updatedAt: NSDate!
    var viewsCount: Int = -1
    
    
    var createdAt: NSDate!
    var discussion: String = ""
    var image: String = ""
    var oldId: Int = -1
    var permalink: String = ""
    var featured: Bool = false
    var username: String = ""
    var categoryTitle: String = ""
    var ticcklesCount: Int = -1
    var repliesCount: Int = -1
    var userPresence: String = ""
    var userOldId: Int = -1
    var userAvatarUrl: String = ""
}
